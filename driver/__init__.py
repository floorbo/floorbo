#!/usr/bin/python

class driver:

    def __init__(self):
        #Diameter of robot in MM
        self.diameter = 250

        #Inlet width in MM
        self.inlet_width = 140

        #Inlet offset in MM
        self.inlet_offset = 70

        #Bumper travel in MM
        self.bumper_travel = 5

        #Battery full recharge time in mins
        self.battery_recharge_time = 360

        #Battery capacity in MAH
        self.battery_capacity = 10000

        #Robot power draw in MAH
        self.battery_draw = 4000

    #======================================================

    def activate_movement_motors(self):
        print("Activate movement motors")

    def deactivate_movement_motors(self):
        print("Deactivate movement motors")
    
    def move_forward(self):
        print("Move forward")

    def move_backwards(self):
        print("Move backward")

    def turn_left(self,degrees):
        print("Turn left")

    def turn_right(self,degrees):
        print("Turn right")

    def activate_vacuum(self):
        print("Activate vacuum")

    def deactivate_vacuum(self):
        print("Deactivate vacuum")


    #TODO: Report drop, bump detection and main button back to main process
        
