#!/bin/python
import sqlite3
import json

class db_record:
    def __init__(self,db_conn,table):
        self.db = db_conn
        self.table = table
        self.data = self.db.get_default_record(self.table)

    def set(self,column,value=None):
        if(column in self.data):
            use_value = value
            col_type = self.data[column]['type']

            if(col_type == "json"): use_value = json.dumps(use_value)

            self.data[column]['value'] = use_value
        else: raise AttributeError('No such column '+column+' in '+self.table)

    def get(self,column):
        if(column in self.data):
            use_value = self.data[column]['value']
            col_type = self.data[column]['type']

            if(col_type == "json"):
                use_value = json.loads(use_value)
            elif(col_type != "text" and col_type != "int"):
                use_value = self.db.fetch_one(col_type,"id=?",[use_value])

            return use_value
        else: raise AttributeError('No such column '+column+' in '+self.table)

    def delete(self):
        if(self.data[id] != None):
            self.db.raw_query("DELETE FROM "+self.table+" WHERE id=?",[self.data[id]])
        else: raise AttributeError('Cannot delete a record which has no id')

    def persist(self):
        if(self.data[id] != None):
            #Update
            query_data = []
            query = ""
            for k,v in self.data.items():
                if(k != "id"):
                    if(query != ""): query = query+","
                    query = query+" "+k+"=?"
                    query_data.append(v)
            query = "UPDATE "+self.table+" SET"+query+" WHERE id=?"
            query_data.append(self.data['id'])
            self.db.raw_query(query,query_data)
        else:
            #Insert
            query_data = []
            query_columns = ""
            query_values = ""
            for k,v in self.data.items():
                if(k != "id"):
                    if(query_columns != ""):
                        query_columns = query_columns+","
                        query_values = query_values+","
                    query_columns = query_columns+k
                    query_values = query_values+"?"
                    query_data.append(v)
            query = "INSERT INTO "+self.table+" ("+query_columns+") VALUES ("+query_values+")"
            self.db.raw_query(query,query_data)
            self.set('id',self.db.last_insert_id())
        

#====================================================


class db:

    def __init__(self,db_file,db_structure=None):
        self.db_file = db_file
        self.db_structure = db_structure
        self.conn = None
        self.open()
        self.ensure_structure()

    #====================================================
    #Connection
    
    def open(self):
        self.conn = sqlite3.connect(self.db_file)
        self.conn.row_factory = sqlite3.Row
        self.reset_cursor()

    def close(self):
        self.conn.close()
        self.conn = None

    def is_connected(self):
        if(self.conn != None):
            return True
        else: return False

    #====================================================
    #Queries

    def reset_cursor(self):
        self.cursor = self.conn.cursor()

    def raw_query(self,sql,values=[]):
        self.reset_cursor()
        self.cursor.execute(sql,tuple(values))
        self.conn.commit()
        return self.cursor

    def num_rows(self):
        return len(self.cursor.fetchall())

    def get_rows(self):
        return self.cursor

    def last_insert_id(self):
        return self.cursor.lastrowid

    #====================================================
    #Structure

    def ensure_structure(self):
        if(self.db_structure != None):
            dbschema = dict(self.db_structure)
            exist_schema = {}
            deleted_cols = {}
            get_tables = self.raw_query("SELECT * FROM sqlite_master WHERE type='table' AND name != 'sqlite_sequence'")
            for dbtab in get_tables:
                exist_schema[dbtab['name']] = {}
                deleted_cols[dbtab['name']] = {}
                get_cols = self.raw_query("pragma table_info("+dbtab['name']+")")
                for col in get_cols:
                    exist_schema[dbtab['name']][col[1]] = {}
                    exist_schema[dbtab['name']][col[1]]['type'] = col[2]
                    exist_schema[dbtab['name']][col[1]]['null'] = col[3]
                    exist_schema[dbtab['name']][col[1]]['def'] = col[4]
                    exist_schema[dbtab['name']][col[1]]['pk'] = col[5]

            for table in exist_schema:
                if(table in dbschema):
                    for col in exist_schema[table]:
                        if(col in dbschema[table]):
                            del dbschema[table][col]
                        else:
                            other_cols = ''
                            for othcol in exist_schema[table]:
                                if(othcol != col and othcol not in deleted_cols[table]):
                                    if(other_cols != ""): other_cols = other_cols+", "
                                    other_cols = other_cols+othcol

                            self.raw_query("CREATE TABLE "+table+"_backup AS SELECT "+other_cols+" FROM "+table)
                            self.raw_query("DROP TABLE "+table)
                            self.raw_query("ALTER TABLE "+table+"_backup RENAME TO "+table)
                            deleted_cols[table][col] = {}

                    if(len(dbschema[table]) > 0):
                        for col in dbschema[table]:
                            if(dbschema[table][col]['type'] == "text" or dbschema[table][col]['type'] == "json"):
                                use_type = ' TEXT'
                            else: use_type = ' INTEGER'

                            if(dbschema[table][col]['null'] == False):
                                use_null = ' NOT NULL'
                            else: use_null = ''

                            if(dbschema[table][col]['def'] != None and dbschema[table][col]['def'] != ""):
                                use_def = dbschema[table][col]['def']
                                if(use_type == " INTEGER"):
                                    use_def = " DEFAULT "+str(use_def)
                                else:
                                    use_def = " DEFAULT '"+str(use_def)+"'"
                            else: use_def = ''

                            if(dbschema[table][col]['pk'] == True):
                                use_pk = ' PRIMARY KEY'
                                if(dbschema[table][col]['ai'] == True): use_pk = use_pk+' AUTOINCREMENT'
                            else: use_pk = ''

                            self.raw_query("ALTER TABLE "+table+" ADD "+str(col)+use_type+use_null+use_def+use_pk)

                    del dbschema[table]

                else:
                    self.raw_query("DROP TABLE "+table)

            if(len(dbschema) > 0):
                for table in dbschema:
                    create_table = ''

                    for col in dbschema[table]:
                        if(dbschema[table][col]['type'] == "text" or dbschema[table][col]['type'] == "json"):
                            use_type = ' TEXT'
                        else: use_type = ' INTEGER'

                        if(dbschema[table][col]['null'] == False):
                            use_null = ' NOT NULL'
                        else: use_null = ''

                        if(dbschema[table][col]['def'] != None and dbschema[table][col]['def'] != ""):
                            use_def = dbschema[table][col]['def']
                            if(use_type == " INTEGER"):
                                use_def = " DEFAULT "+str(use_def)
                            else:
                                use_def = " DEFAULT '"+str(use_def)+"'"
                        else: use_def = ''

                        if(dbschema[table][col]['pk'] == True):
                            use_pk = ' PRIMARY KEY'
                            if(dbschema[table][col]['ai'] == True): use_pk = use_pk+' AUTOINCREMENT'
                        else: use_pk = ''

                        if(create_table != ""): create_table = create_table+', '
                        create_table = create_table+col+use_type+use_null+use_def+use_pk

                    self.raw_query("CREATE TABLE "+table+" ("+create_table+")")

            del exist_schema
            del deleted_cols

    #====================================================
    #Fetch helpers

    def fetch_one(self,table,expression=None,expression_data=[]):
        results = self.fetch(table,expression,expression_data)
        if(len(results) > 0):
            return results[0]
        else: return None

    def fetch_all(self,table,expression=None,expression_data=[]):
        return self.fetch(table,expression,expression_data)

    def fetch(self,table,expression=None,expression_data=[]):
        query = "SELECT * FROM "+table
        if(expression != None): query = query+" WHERE "+expression

        self.raw_query(query,expression_data)
        results = []
        for row in self.get_rows():
            instance_record = db_record(self,table)
            for col in self.db_structure[table]:
                instance_record.set(col,row[str(col)])
            results.append(instance_record)

        return results
    

    def get_default_record(self,table):
        default_record = {}
        table_struct = self.db_structure[table]

        for col in table_struct:
            if(table_struct[col]['def'] != None and table_struct[col]['def'] != ""):
                use_value = table_struct[col]['def']
                if(table_struct[col]['type'] == "text" or table_struct[col]['type'] == "json"):
                    use_value = str(use_value)
                else: use_value = int(use_value)
            else: use_value = None
            default_record[col] = {'value': use_value, 'type': table_struct[col]['type']}

        return default_record


    #====================================================
