#!/usr/bin
import os
import db
import json

class floorbo():

    def __init__(self,driver):
        print("Floorbo daemon started")
        self.data_dir = self.get_data_dir()
        self.db = self.open_db()
        self.driver = driver
        self.setup_power_management()

    def get_working_dir(self):
        return os.path.dirname(os.path.abspath(__file__))+'/';

    def get_data_dir(self):
        data_dir = str(os.path.expanduser("~"))+'/.floorbo'
        if(os.path.exists(data_dir) == False): os.mkdir(data_dir)
        return data_dir+'/'

    def open_db(self):
        dbschema_file = open(self.get_working_dir()+'dbschema','r')
        dbschema = json.loads(dbschema_file.read())
        dbschema_file.close()
        print("Database opened")
        return db.db(self.data_dir+'floorbo.db',dbschema)

    def current_timestamp(self):
        return int(datetime.datetime.now().timestamp())

    #================================================================
    #CHARGE

    def setup_power_management(self):
        self.run_time = 0
        
        battery_stats = self.db.fetch_one("battery","id=1")
        if(battery_stats == None):
            battery_stats = db.db_record(self.db,"battery")
            battery_stats.set('percent',100)
            battery_stats.set('charge_cycles',0)
            battery_stats.persist()

        


    #================================================================
    #JOB

    def load_job_schedule(self):
        self.job_schedule = self.db.fetch_all('job_schedule')

    #================================================================

