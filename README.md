# floorbo

The code for the Floorbo bot including:
* The logic required to direct the robot and navigate rooms.
* The web app to control the bot, schedule jobs, run updates, change settings etc