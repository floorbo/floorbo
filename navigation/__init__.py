#!/bin/python
from math import cos, sin, radians, pi

#Right (90)  = +X
#Down (180)  = -Y
#Left (270)  = -X
#Up (360)    = +Y

def calculate_position(previous_x, previous_y, distance_travelled, bearing_degrees):
    theta_rad = pi/2 - radians(bearing_degrees)
    new_x = previous_x + (distance_travelled * cos(theta_rad))
    new_y = previous_y + (distance_travelled * sin(theta_rad))
    return {'x': round(new_x,2), 'y': round(new_y,2)}

'''
current.append(calculate_position(current[0]['x'],current[0]['y'],distance,90))
current.append(calculate_position(current[1]['x'],current[1]['y'],distance,180))
current.append(calculate_position(current[2]['x'],current[2]['y'],distance,270))
current.append(calculate_position(current[3]['x'],current[3]['y'],distance,0))
'''
